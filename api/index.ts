import express from 'express';
import { createServer } from 'http';
const app = express();
const server = createServer(app);
import WebSocket, { WebSocketServer } from 'ws';
const wss = new WebSocket.Server({ server });
const port = 8080;
wss.on('connection', (ws: WebSocket) => {
    console.log('WebSocket connection established');

    ws.on('message', (message: string) => {
        console.log('Received message:', message);
        // Echo the received message back to the client
        ws.send(`Server received: ${message}`);
    });

    ws.on('close', () => {
        console.log('Client disconnected');
    });

    ws.on('error', (error) => {
        console.error('WebSocket error:', error);
    });
});

// Example HTTP route
app.get('/', (req, res) => {
    res.send('Hello from Express!');
});

// Start the server
server.listen(port, () => {
    console.log(`Server is listening on ${port}`);
});

module.exports = app;